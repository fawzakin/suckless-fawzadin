# THIS REPO IS DEPRECATED
Please use the individual suckless tools provided by this account.

# suckless-fawzadin
My build of some suckless utilities such as dmenu, slock, st, and tabbed. All patches are included in each program's directory. "install_suckless.sh" will install all programs available (including dwmblocks) in this repo. 

## dmenu
Dynamic Menu for anything from running programs to inquiring input for web search. Required for my build of dwm and st. 

### patches:
- fuzzyhighlight
- fuzzymatch
- grid
- gridnav
- highpriority\*
- line height
- no sort
- numbers

\* Doesn't seem to work. Rename dmenu.hp.c and uncomment [SchemeHP] in config.def.h to enable highpriority.

## slock
Simple screen locker. I use the dwmlogo patch to add a cute lock icon on the lockscreen. This locker has less dependencies than the one I once used previously (betterlockscreen).
\[UPDATE May 4th 2021: due to the fact that it is required to kill the compositor to not make slock look weird and it is currenly broken on my system with "segmentation fault" error, I've replaced it with i3lock for the time being.\]
### patches:
- colormessage
- dpms
- dwmlogo
- control clear
- mediakeys

## st
Simple Terminal. I took some part from Luke Smith's build to get the dmenu integration feature. This would make it "literally the best terminal emulator ever." This is the terminal of choice for my build of dwm other than Alacritty.

There are multiple colorscheme you can choose as a patch.  Make sure the color part of `config.def.h` is unaltered from what comes in this repo. Otherwise, you have to manually change the color yourself.

TIPS: Use `-u` flag to keep the original file as backup.

### patches:
- alpha
- anysize
- blinking cursor
- bold is not bright
- clipboard
- dynamic cursor color
- externalpipe (+eternal) 
- font2
- ligature (alpha-scrollback) 
- newterm (alt+shift+enter to open new terminal with same pwd)
- scrollback (+mouse +mouse-altscreen) 
- vertcenter
- workingdir

### scripts (use dmenu):
- copyout (copy output of run commands. alt+o) 
- urlhandler (copy or follow any links on screen. alt+y to copy, alt+l to follow) 

## tabbed
Give tabbing to any xembeddable programs such as Alacritty or st. My build of dwm only uses tabbed for the scratchpad terminal. 

### patches:
- clientnumber

## other
I also include block.def.h for dwmblocks. I don't modify this program so it's better to just pull it from someone else's repo and replace block.def.h with my own. 
