//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/	 	/*Update Interval*/	/*Update Signal*/

    {" ", "/bin/sh -c $HOME/.local/share/dwm/status/cpu_usage",		5,	0},

    {" ", "/bin/sh -c $HOME/.local/share/dwm/status/memory",	        5,	0},

    {" ", "/bin/sh -c $HOME/.local/share/dwm/status/cpu_temp",		5,	0},    
    
    {" ", "/bin/sh -c $HOME/.local/share/dwm/status/volume",		0,	10},

    {" ", "/bin/sh -c $HOME/.local/share/dwm/status/battery",		5,	0},

    {" ", "/bin/sh -c $HOME/.local/share/dwm/status/clock",		60,	15},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim = '/';
