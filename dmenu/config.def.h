/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
static int fuzzy = 1;                      /* -F  option; if 0, dmenu doesn't use fuzzy matching     */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
        "JetBrains Mono Medium:size=11:antialias=true:autohint=true"
};
static const char *prompt      = NULL;      /* -p  option; prompt to the left of input field */
static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
/* Blue color scheme */        
/*BlueStart
	[SchemeNorm] = { "#d7d7d7", "#22252c" },
	[SchemeSel] = { "#eeeeee", "#3465a4" },
	[SchemeSelHighlight] = { "#d7d7d7", "#000000" },
	[SchemeNormHighlight] = { "#729fcf", "#000000" },
	[SchemeOut] = { "#000000", "#d7d7d7" },
	//[SchemeHp] = { "#bbbbbb", "#333333" }
BlueEnd*/

/* Red color scheme */
/*RedStart*/
	[SchemeNorm] = { "#d7d7d7", "#262626" },
	[SchemeSel] = { "#eeeeee", "#ef2929" },
	[SchemeSelHighlight] = { "#d7d7d7", "#000000" },
	[SchemeNormHighlight] = { "#cc241d", "#000000" },
	[SchemeOut] = { "#000000", "#d7d7d7" },
	//[SchemeHp] = { "#bbbbbb", "#333333" }
/*RedEnd*/

/* Nord color scheme */
/*NordStart
	[SchemeNorm] = { "#d8dee9", "#3b4252" },
	[SchemeSel] = { "#eeeeee", "#68759f" },
	[SchemeSelHighlight] = { "#d8dee9", "#000000" },
	[SchemeNormHighlight] = { "#68759f", "#000000" },
	[SchemeOut] = { "#000000", "#d8dee9" },
	//[SchemeHp] = { "#bbbbbb", "#333333" }
NordEnd*/

};
/* -l and -g options; controls number of lines and columns in grid if > 0 */
static unsigned int lines      = 0;
static unsigned int columns    = 0;
/* -h option; minimum height of a menu line */
static unsigned int lineheight = 22;
static unsigned int min_lineheight = 22;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";
