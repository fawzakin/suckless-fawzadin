#!/bin/sh

currentsl="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

echo "Installing dmenu..."
cd $currentsl/dmenu

printf "> Choose colorscheme: Red, Blue, or Nord (R/b/n)? "
read -r REPLY
case $REPLY in
    b|B ) echo "> Picked Blue colorscheme."
        sed -i -e 's|BlueStart|BlueStart*/|g' -e 's|BlueEnd|/*BlueEnd|g' config.def.h  
        sed -i -e 's|RedStart*/|RedStart|g' -e 's|/*RedEnd|RedEnd|g' config.def.h
        autocolor=blue
        ;;
    n|N ) echo "> Picked Nord colorscheme."
        sed -i -e 's|NordStart|NordStart*/|g' -e 's|NordEnd|/*NordEnd|g' config.def.h  
        sed -i -e 's|RedStart*/|RedStart|g' -e 's|/*RedEnd|RedEnd|g' config.def.h
        autocolor=nord
        ;;
    * ) echo "> Picked Red colorscheme."
        autocolor=red
        ;;
esac


sudo make install

# Slock may be broken on some distro.
#echo "Installing slock..."
#cd $currentsl/slock
#sudo make install

echo "Installing st..."
cd $currentsl/st

if [ "$autocolor" = "blue" ]; then
    sed -i -e 's|#222222|#1b1d24|g' config.def.h  
elif [ "$autocolor" = "nord" ]; then
    patch -b config.def.h color-nord.patch
fi

sudo make install

echo "Installing tabbed..."
cd $currentsl/tabbed

if [ "$autocolor" = "blue" ]; then
    sed -i -e 's|BlueStart|BlueStart*/|g' -e 's|BlueEnd|/*BlueEnd|g' config.def.h  
    sed -i -e 's|RedStart*/|RedStart|g' -e 's|/*RedEnd|RedEnd|g' config.def.h
elif [ "$autocolor" = "nord" ]; then
    sed -i -e 's|NordStart|NordStart*/|g' -e 's|NordEnd|/*NordEnd|g' config.def.h  
    sed -i -e 's|RedStart*/|RedStart|g' -e 's|/*RedEnd|RedEnd|g' config.def.h
fi

sudo make install

echo "Installing dwmblocks..."
cd $currentsl
git clone https://gitlab.com/dwt1/dwmblocks-distrotube.git
cp $currentsl/blocks.def.h $currentsl/dwmblocks-distrotube/blocks.def.h
cd $currentsl/dwmblocks-distrotube

printf "Do you want to remove battery reading (recommended if you are on Desktop) (y/N)? "
read -r REPLY
[ "$REPLY" = "y" ] || [ "$REPLY" = "Y" ] && sed -i -e 13's/.*/\/\/&/' blocks.def.h

printf "Do you want to remove temperature reading (recommended if you are on Virtual Machine) (y/N)? " 
read -r REPLY
[ "$REPLY" = "y" ] || [ "$REPLY" = "Y" ] && sed -i -e 9's/.*/\/\/&/' blocks.def.h

sed -i -e '/pkg-config/s/$/ -lX11/' Makefile

cp blocks.def.h blocks.h
sudo make install

echo "All operations for suckless tools are done!"

