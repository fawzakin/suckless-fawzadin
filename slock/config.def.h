/* user and group to drop privileges to */
static const char *user  = "nobody";
static const char *group = "nobody";

static const char *colorname[NUMCOLS] = {
	[BACKGROUND] =  "#E5E5E5",     /* background */
	[INIT] =        "#222222",     /* after initialization */
	[INPUT] =       "#FFD700",   /* during input */
	[FAILED] =      "#CC241D",   /* wrong password */
};
/* Grey: E5E5E5 Blue: #3498DB Red: #CC241D Gold: #FFD700 */

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 1;

/* allow control key to trigger fail on clear */
static const int controlkeyclear = 0;

/* time in seconds before the monitor shuts down */
static const int monitortime = 30;

/* default message */
static const char * message = "Enter Password to Continue";

/* text color */
static const char * text_color = "#FFFFFF";

/* text size (must be a valid size) */
static const char * font_name = "6x13";

/* insert grid pattern with scale 1:1, the size can be changed with logosize */
static const int logosize = 40;
static const int logow = 10;	/* grid width and height for right center alignment*/
static const int logoh = 15;

static XRectangle rectangles[12] = {
	/* x	y	w	h */
/*
    {1,0,1,2},
    {2,0,2,1},
    {4,0,1,2},
    {0,2,2,3},
    {2,2,2,1},
    {2,4,2,1},
    {4,2,2,3},
*/
    {3,0,4,1}, // 1
    {3,1,1,1}, // 1a
    {6,1,1,1}, // 1b
    {2,1,1,4}, // 2
    {7,1,1,4}, // 3
    {1,5,8,1}, // 4
    {0,6,4,7}, // 5
    {4,6,2,2}, // 6
    {4,12,2,1}, // 7
    {6,6,4,7}, // 8
    {1,13,8,1}, // 9
    {2,14,6,1}, // 10
};
